$(document).ready(init);

function init(){
	menuSelection();
	route();
	$(window).on('hashchange', route);
}

function menuSelection(){
	_.each(['#usersMenu','#ordersMenu','#goodsMenu'],function(id){
		if( $(id).hasClass('active') )
			$(id).removeClass('active');
	});
}

function route(){
	menuSelection();
	switch(location.hash){
		case "":
		  $('#container').html('<h1>Please select section above</h1>');
		  break;
		case "#/users":
		  $('#usersMenu').addClass('active');
		  showUsers();
		  break;
		case "#/orders":
		  $('#ordersMenu').addClass('active');
		  showOrders();
		  break;
		case "#/goods":
		  $('#goodsMenu').addClass('active');
		  showGoods();
		  break;
	}

    if(location.hash.indexOf('#/orders/delete/')==0)
	if(/#\/orders\/delete\/([^,]*)/g.exec(location.hash)[1]!=0){
		var orderId = /#\/orders\/delete\/([^,]*)/g.exec(location.hash)[1];
		
		$.get('./rest.php?a=deleteOrder&id='+orderId)
		.always(function(){location.href = '#/orders';})

		
	}
		  

}

function showUsers(){
	$.get('./rest.php?a=showUsers')
	.done(function(data){
		var table = "<table class=\"table\">\
      <thead>\
        <tr>\
          <th>id</th>\
          <th>First Name</th>\
          <th>Last Name</th>\
          <th>Email</th>\
          <th>Created At</th>\
        </tr>\
      </thead>\
      <tbody>";

      for(var index in data){
      	var user = data[index];
		table += "<tr>\
          <td>"+user['id']+"</td>\
          <td>"+user['firstName']+"</td>\
          <td>"+user['lastName']+"</td>\
          <td>"+user['email']+"</td>\
          <td>"+user['updated_at']+"</td>\
        </tr>";
      }
        
      table += "</tbody>\
	    </table>";
     $('#container').html(table);
	})
	.fail(function(){ $('#container').html('<h1>Failed to load Users</h1>'); })
}

function showOrders(){
	$.get('./rest.php?a=showOrders')
	.done(function(data){
		var table = "<table class=\"table\">\
      <thead>\
        <tr>\
          <th>id</th>\
          <th>User Id</th>\
          <th>Goods</th>\
          <th>Deleted</th>\
          <th>Created At</th>\
          <th></th>\
        </tr>\
      </thead>\
      <tbody>";

      for(var index in data){
      	var user = data[index];
		table += "<tr>\
          <td>"+user['id']+"</td>\
          <td>"+user['user']+"</td>\
          <td>"+user['goods']+"</td>";
          if(user['deleted']=='0')
	          table += "<td><span class=\"glyphicon glyphicon-remove\"></span></td>";
	      else
	      	  table += "<td><span class=\"glyphicon glyphicon-ok\"></span></td>";
          table += "<td>"+user['updated_at']+"</td>\
          <td>\
			<a href=\"#/orders/delete/"+user['id']+"\" class=\"btn btn-danger\"><span class=\"glyphicon glyphicon-trash\"></span>&nbsp;Remove</a>\
          </td>\
        </tr>";
      }
        
      table += "</tbody>\
	    </table>";
     $('#container').html(table);
	})
	.fail(function(){ $('#container').html('<h1>Failed to load Orders</h1>'); })
}

function showGoods(){
	$.get('./rest.php?a=showGoods')
	.done(function(data){
		var table = "<table class=\"table\">\
      <thead>\
        <tr>\
          <th>id</th>\
          <th>Title</th>\
          <th>Price</th>\
          <th>Deleted</th>\
          <th>Created At</th>\
        </tr>\
      </thead>\
      <tbody>";

      for(var index in data){
      	var user = data[index];
		table += "<tr>\
          <td>"+user['id']+"</td>\
          <td>"+user['title']+"</td>\
          <td>"+user['price']+"</td>";
          if(user['deleted']=='0')
	          table += "<td><span class=\"glyphicon glyphicon-remove\"></span></td>";
	      else
	      	  table += "<td><span class=\"glyphicon glyphicon-ok\"></span></td>";
          table += "<td>"+user['updated_at']+"</td>\
        </tr>";
      }
        
      table += "</tbody>\
	    </table>";
     $('#container').html(table);
	})
	.fail(function(){ $('#container').html('<h1>Failed to load Goods</h1>'); })
}