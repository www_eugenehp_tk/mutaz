<?php

include '../include/mysql_api.php';
// TODO: rest interface here

$route = $_GET['a'];

if($route === "showUsers"){	
	header('Content-type: application/json');
	print(
		json_encode(
			readUser()
		)
	);
}else if($route === "showGoods"){
	header('Content-type: application/json');
	print(
		json_encode(
			readGood()
		)
	);
}else if($route === "showOrders"){
	header('Content-type: application/json');
	print(
		json_encode(
			readOrder()
		)
	);
}else if($route === "deleteOrder"){
	if(isset($_GET['id'])){
		$id = $_GET['id'];
		echo "$id delete order";
	}else{
		throw new Exception('You need to have a valid orderId, so you can delete it, for example please use &id=10');
	}
}

?>