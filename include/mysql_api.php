<?php
// This file describes all of the basic MySQL routine that is being used in the project

//returns the connection for the mySQL
function connect() {

	// default settings for the MySQL
	$HOST = "localhost";
	$USER = "root";
	$PASS = "";
	$DB = "mutaz";

	$connection = mysql_connect($HOST,$USER,$PASS);
	mysql_select_db($DB,$connection);

	return $connection;
}

function close($connection) {
	$errno = intval(mysql_errno($connection));
	if($errno>0)
		echo "$errno: " . mysql_error($connection) . "\n";
	mysql_close($connection);
}


/*
USERS
*/
function signup($email, $password, $firstName, $lastName) {
	$connection = connect();
	$options = [
		'cost' => 11,
		'salt' => mcrypt_create_iv(22, MCRYPT_DEV_URANDOM),
	];
	$hash = password_hash($password, PASSWORD_BCRYPT, $options);

	$sql = "INSERT INTO `mutaz`.`users` (`id`, `email`, `hash`, `firstName`, `lastName`, `updated_at`) VALUES ('0', '$email', '$hash', '$firstName', '$lastName', CURRENT_TIMESTAMP);";
	$result = mysql_query($sql,$connection);

	close($connection);

	return $result === 1;
}

function signin($email,$password) {
	$connection = connect();

	$sql = "SELECT * FROM `mutaz`.`users` WHERE `email`='$email';";
	$result = mysql_query($sql,$connection);
	$user = mysql_fetch_assoc($result);

	$hash = $user['hash'];
	$valid = password_verify($password,$hash);

	session_start();
	close($connection);

	return $valid;
}

function signout() {
	session_destroy();

	return true;
}

function readUser($id = null) {
	$connection = connect();
	$id = intval($id);
	
	$where = "";
	if($id)
		$where = "WHERE `goods`.`id` = $id";

	$sql = "SELECT `id`,`email`,`firstName`,`lastName`,`updated_at` FROM `users` $where;";
	$result = mysql_query($sql,$connection);

	$array = array();

	while(($row =  mysql_fetch_assoc($result))) {
		$array[] = $row;
	}

	close($connection);

	return $array;
}

/*
GOODS
*/
function createGood($title,$price) {
	$connection = connect();
	$price = intval($price);
	
	$sql = "INSERT INTO `mutaz`.`goods` (`id`, `title`, `price`, `deleted`, `updated_at`) VALUES ('0', '$title', $price, FALSE, CURRENT_TIMESTAMP);";
	$result = mysql_query($sql,$connection);

	close($connection);

	return $result === 1;
}

function readGood($id = null) {
	$connection = connect();
	$id = intval($id);
	
	$where = "";
	if($id)
		$where = "WHERE `goods`.`id` = $id";

	$sql = "SELECT * FROM `goods` $where;";
	$result = mysql_query($sql,$connection);

	$array = array();

	while(($row =  mysql_fetch_assoc($result))) {
		$array[] = $row;
	}

	close($connection);

	return $array;
}

function updateGood($id,$updateArray) {
	$connection = connect();
	$id = intval($id);

	$update = "";
	foreach($updateArray as $key=>$value) {
		if($key === "deleted"){
			if(boolval($value)) $value = 1; else $value = 0;
		}
		$update .= "`$key` = '$value', ";
	}
	$update = rtrim($update, ", ");
	
	// $update = "`title` = 'PlayStation #10', `price` = '101', `deleted` = '0'";
	$sql = "UPDATE `mutaz`.`goods` SET $update WHERE `goods`.`id` = $id;";
	$result = mysql_query($sql,$connection);

	close($connection);

	return $result === 1;
}

function deleteGood($id) {
	$connection = connect();
	$id = intval($id);
	
	$sql = "UPDATE `mutaz`.`goods` SET `deleted` = TRUE WHERE `goods`.`id` = $id;";
	$result = mysql_query($sql,$connection);

	close($connection);

	return $result === 1;
}

/*
ORDERS
*/
function createOrder($user,$goods) {
	$connection = connect();
	
	$sql = "INSERT INTO `mutaz`.`orders` (`id`, `user`, `goods`, `deleted`, `updated_at`) VALUES ('0', '$user', '$goods', FALSE, CURRENT_TIMESTAMP);";
	$result = mysql_query($sql,$connection);

	close($connection);

	return $result === 1;
}

function readOrder($id = null) {
	$connection = connect();
	$id = intval($id);
	
	$where = "";
	if($id)
		$where = "WHERE `orders`.`id` = $id";

	$sql = "SELECT * FROM `orders` $where;";
	$result = mysql_query($sql,$connection);

	$array = array();

	while(($row =  mysql_fetch_assoc($result))) {
		$array[] = $row;
	}

	close($connection);

	return $array;
}

function updateOrder($id,$updateArray) {
	$connection = connect();
	$id = intval($id);

	$update = "";
	foreach($updateArray as $key=>$value) {
		if($key === "deleted"){
			if(boolval($value)) $value = 1; else $value = 0;
		}
		$update .= "`$key` = '$value', ";
	}
	$update = rtrim($update, ", ");
	
	$sql = "UPDATE `mutaz`.`orders` SET $update WHERE `orders`.`id` = $id;";
	$result = mysql_query($sql,$connection);

	close($connection);

	return $result === 1;
}

function deleteOrder($id) {
	$connection = connect();
	$id = intval($id);
	
	$sql = "UPDATE `mutaz`.`orders` SET `deleted` = TRUE WHERE `orders`.`id` = $id;";
	$result = mysql_query($sql,$connection);

	close($connection);

	return $result === 1;
}

?>